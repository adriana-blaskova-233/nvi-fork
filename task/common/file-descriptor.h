#ifndef COMMON_FILE_DESCRIPTOR_H
#define COMMON_FILE_DESCRIPTOR_H

#include <errno.h>
#include <poll.h>
#include <string_view>
#include <type_traits>
#include <unistd.h>
#include <vector>

#include "system-error.h"

namespace common {

class FileDescriptor {
    template<typename T>
    constexpr static bool isChar = std::is_same_v<T, char>
                                || std::is_same_v<T, signed char>
                                || std::is_same_v<T, unsigned char>
                                || std::is_same_v<T, std::byte>;

public:
    using OnClose = int(*)(int);
    enum {Invalid = -1};

    ~FileDescriptor()
    {
        if (isValid()) {
          close();
        }
    }

    explicit FileDescriptor()
        : _fd(-1)
    {}

    explicit FileDescriptor(int fd)
        : _fd(fd)
    {}

    FileDescriptor(const FileDescriptor &) = delete;
//    FileDescriptor *operator=(const FileDescriptor &) = delete;

    FileDescriptor(FileDescriptor &&other) : _fd(std::exchange(other._fd,  -1)) {
    }

    FileDescriptor &operator=(FileDescriptor other) {
        swap(other);
        return *this;
    }

    void swap(FileDescriptor &other) {
        using std::swap;
        swap(_fd, other._fd);
    }

    // TODO: add other methods

    bool isValid() const {
        return _fd != -1;
    }

    explicit operator bool() const {
        return isValid();
    }

    /**
     * @brief Read a data from the file descriptor.
     *
     * @param buffer Fills the buffer up to the its capacity.
     * @return ssize_t The number of filled characters.
     */
    ssize_t read(std::string &buffer) const {
        return _readWithResize(buffer);
    }

    /**
     * @brief Read a data from the file descriptor.
     *
     * @tparam Char Must be one of char, signed char, unsigned char, std::byte.
     * @param buffer Fills the buffer up to the its capacity.
     * @return ssize_t The number of filled characters.
     */
    template<
        typename Char,
        typename = std::enable_if_t<isChar<Char>>
    >
    ssize_t read(std::vector<Char> &buffer) const {
        return _readWithResize(buffer);
    }

    /**
     * @brief Read a data from the file descriptor.
     *
     * @tparam Char Must be one of char, signed char, unsigned char, std::byte.
     * @tparam N The capacity of the buffer.
     * @param buffer Fills the buffer up to the its capacity.
     * @return ssize_t The number of filled characters.
     */
    template<
        typename Char,
        std::size_t N,
        typename = std::enable_if_t<isChar<Char>>
    >
    ssize_t read(std::array<Char, N> &buffer) const {
        return _read(buffer);
    }

    /**
     * @brief Write a data to the file descriptor.
     *
     * @param buffer The buffer of data to be written.
     * @return ssize_t The number of written characters.
     */
    ssize_t write(std::string_view buffer) const {
        return _write(buffer);
    }

    /**
     * @brief Write a data to the file descriptor.
     *
     * @tparam Char Must be one of char, signed char, unsigned char, std::byte.
     * @param buffer The buffer of data to be written.
     * @return ssize_t The number of written characters.
     */
    template<
        typename Char,
        typename = std::enable_if_t<isChar<Char>>
    >
    ssize_t write(const std::vector<Char> &buffer) const {
        return _write(buffer);
    }

    /**
     * @brief Read a data from the file descriptor.
     *
     * @tparam Char Must be one of char, signed char, unsigned char, std::byte.
     * @tparam N The capacity of the buffer.
     * @param buffer Fills the buffer up to the its capacity.
     * @return ssize_t The number of written characters.
     */
    template<
        typename Char,
        std::size_t N,
        typename = std::enable_if_t<isChar<Char>>
    >
    ssize_t write(const std::array<Char, N> &buffer) const {
        return _write(buffer);
    }

    void close() {
        _onClose(_fd);
    }

    pollfd toPoll() const {
        return {
            descriptor(),
            POLLIN,
            0
        };
    }

    friend bool operator==(const FileDescriptor &lhs, const FileDescriptor &rhs)
    {
        return lhs._fd == rhs._fd;
    }

    friend bool operator!=(const FileDescriptor &lhs, const FileDescriptor &rhs)
    {
        return !(lhs == rhs);
    }

    friend bool operator<(const FileDescriptor &lhs, const FileDescriptor &rhs)
    {
        return lhs._fd < rhs._fd;
    }

    friend bool operator<=(const FileDescriptor &lhs, const FileDescriptor &rhs)
    {
        return !(lhs > rhs);
    }

    friend bool operator>(const FileDescriptor &lhs, const FileDescriptor &rhs)
    {
        return rhs < lhs;
    }

    friend bool operator>=(const FileDescriptor &lhs, const FileDescriptor &rhs)
    {
        return !(lhs < rhs);
    }

protected:
    int descriptor() const {
        return _fd;
    }

    void setOnClose(OnClose onClose) {
        if (!onClose)
            throw std::runtime_error("invalid OnClose handler");
        _onClose = onClose;
    }

private:
    template<typename Buffer>
    ssize_t _readWithResize(Buffer &buffer) const {
        buffer.resize(buffer.capacity());
        auto rv = _read(buffer);
        buffer.resize(rv);
        return rv;
    }

    template<typename Buffer>
    ssize_t _read(Buffer &buffer) const {
        return _process(buffer, ::read, "read");
    }

    template<typename Buffer>
    ssize_t _write(const Buffer &buffer) const {
        return _process(buffer, ::write, "write");
    }

    template<typename Buffer, typename Fn>
    ssize_t _process(Buffer &buffer, Fn fn, const char *what) const {
        do {
            auto rv = fn(descriptor(), buffer.data(), buffer.size());
            if (rv != -1)
                return rv;
        } while (errno == EINTR);
        throw SystemError(what);
    }

    int _fd;
    OnClose _onClose = ::close;
};

} // namespace common

#endif
