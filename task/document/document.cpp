#include "document.h"
#include "doc_priv.h"
#include "error.h"
#include <string_view>
#include <iostream>
#include <sstream>

namespace doc {

void Document::add(std::string path, std::string value) {
    _doc_priv.add(path, value);
}

void Document::setEncoders(std::vector<std::unique_ptr<Encoder>>) {
    throw Error("not implemented");
}

void Document::drop() {
    throw Error("not implemented");
}

//TODO crawl -> accept(visitor)
//  ostream -> visitor.process(node)
std::string Document::status() const {
    auto status_visitor = make_visitor(visitor_e::printing);
    _doc_priv.accept(status_visitor.get());
    return status_visitor->get_the_stuff();
}

} // namespace doc