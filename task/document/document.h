#ifndef DOC_DOCUMENT_H
#define DOC_DOCUMENT_H

#include <string>
#include <vector>
#include <optional>
#include <map>
#include <sstream>

#include "encoder.h"
#include "doc_priv.h"

namespace doc {

/**
 * @brief Document with dynamically created content.
 * 
 */
struct Document {

    /**
     * @brief Add a path with a value into the document.
     * @note If some value is already presented in the document as a part of some key, 
     * 
     * @param path Path in the document
     * @param value Value
     */
    void add(std::string path, std::string value);

    /**
     * @brief Set the Encoders objects which will be applied on all values during exportation.
     *        All previously set encoders will be removed.
     * 
     */
    void setEncoders(std::vector<std::unique_ptr<Encoder>>);

    /**
     * @brief Drop any changes already done with the document.
     * 
     */
    void drop();

    /**
     * @brief Provide human-readable information about the document.
     * 
     * @return std::string 
     */
    std::string status() const;

    void del(std::string path);

    // private:

    doc_priv_t _doc_priv;
};

} // namespace doc

#endif
