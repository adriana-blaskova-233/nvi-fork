#ifndef DOC_COMMAND_H
#define DOC_COMMAND_H

#include <memory>
#include <optional>
#include <string>
#include <vector>

#include "document.h"
#include "generator.h"
#include "../common/logger.h"

namespace doc {

/**
 * @brief Range of two numbers. Possibly goes to infinite on the high end.
 * 
 */
struct Range {
    /**
     * @brief Construct a new Range object with single number.
     * 
     * @param low number
     */
    Range(int low)
        : _low(low)
        , _high(low)
    {}

    /**
     * @brief Construct a new Range object
     * 
     * @param low low number
     * @param high high number
     */
    Range(int low, int high)
        : _low(low)
        , _high(high)
    {}

    /**
     * @brief Construct a new Range object
     * 
     * @param low low number
     * @param nullopt positive infinite number
     */
    Range(int low, std::nullopt_t)
        : _low(low)
    {}

    int low() const {
        return _low;
    }

    std::optional<int> high() const {
        return _high;
    }

    /**
     * @brief Check whether @p number is within the interval of this range both ends included.
     * 
     * @param number 
     * @return true iff the @p number is within the interval of low and high number (including)
     * @return false otherwise
     */
    bool isValid(int number) const {
        return low() <= number && number <= high().value_or(number);
    }

    std::string toString() const {
        if (low() == high())
            return std::to_string(low());
        if (high())
            return std::to_string(low()) + "-" + std::to_string(high().value());
        return std::to_string(low()) + "-inf";
    }

private:
    int _low;
    std::optional<int> _high;
};

using Params = std::vector<std::string>;

struct Command : log::Enable<Command> {
    Command() = default;
    virtual ~Command() = default;

    Command(const Command &) = delete;
    Command(Command &&) = delete;
    Command &operator=(const Command &) = delete;
    Command &operator=(Command &&) = delete;

    std::string_view name() const {
        return _name();
    }

    std::string help() const {
        here() << name();
        return label() + ".\n" + _help();
    }

    std::string process(Document &document, Params params) {
        here() << "processing " << name();
        auto arguments = _arguments();
        if (!arguments.isValid(params.size())) {
            auto name = _name();
            return label() + "; got " + std::to_string(params.size()) + " instead\n";
        }
        return _process(document, std::move(params)).value_or("OK") + '\n';
    }

private:
    std::string label() const {
        auto name = _name();
        return "Command " + std::string(name.begin(), name.end()) + " expects " + _arguments().toString() + " argument(s)";
    }

    virtual std::optional<std::string> _process(Document &, Params) = 0;
    virtual std::string _help() const = 0;
    virtual std::string_view _name() const = 0;
    virtual Range _arguments() const = 0;
};

template<typename T, typename... Args>
std::pair<std::string_view, std::unique_ptr<Command>> makeCmd(Args &&... args) {
    auto cmd = std::make_unique<T>(std::forward<Args>(args)...);
    return {cmd->name(), std::move(cmd)};
}

class Add : public Command {
    std::optional<std::string> _process(Document &document, Params params) override {
        document.add(params[0], params[1]);
        return {};
    }

    std::string _help() const override {
        return "First argument is path, the second argument is data.\n"
               "Path use slashes to separate levels.";
    }

    std::string_view _name() const override {
        return "add";
    }

    Range _arguments() const override {
        return {2};
    }
};

class Export : public Command {
    std::optional<std::string> _process(Document &document, Params param) override {
        // return doc::Generator::create(param[0])->process(document);
        auto gen = doc::make_visitor(str_to_visitor_e(param[0]));
        document._doc_priv.accept(gen.get());
        return gen->get_the_stuff();
    }

    std::string _help() const override {
        return "Export current document to selected format.\n"
                "Currently available xml, json, and yaml.";
    }

    std::string_view _name() const override {
        return "export";
    }

    Range _arguments() const override {
        return {1};
    }

};

class Encode : public Command {
    std::optional<std::string> _process(Document &document, Params params) override {
        std::vector<std::unique_ptr<Encoder>> encoders;
        for (auto &param : params)
            encoders.emplace_back(Encoder::create(param));
        document.setEncoders(std::move(encoders));
        return {};
    }

    std::string _help() const override {
        return "Encode data values with specified encoders.\n"
               "Currently available lowercase, uppercase, hex.";
    }

    std::string_view _name() const override {
        return "encode";
    }

    Range _arguments() const override {
        return {1, std::nullopt};
    }
};

class Status : public Command {
    std::optional<std::string> _process(Document &document, Params) override {
        return document.status();
    }

    std::string _help() const override {
        return "Show status of current document";
    }

    std::string_view _name() const override {
        return "status";
    }

    Range _arguments() const override {
        return {0};
    }
};

class Drop : public Command {
    std::optional<std::string> _process(Document &document, Params) override {
        document.drop();
        return {};
    }

    std::string _help() const override {
        return "Drop current document.";
    }

    std::string_view _name() const override {
        return "drop";
    }

    Range _arguments() const override {
        return {0};
    }
};

} // namespace doc

#endif
