#pragma once

#include "error.h"

#include <map>
#include <memory>
#include <optional>
#include <iostream>
#include <string_view>

namespace doc {

class doc_priv_t;

struct base_visitor_t {
    void do_the_stuff(const doc_priv_t * d, std::string path) { process(d, path); }
    std::string get_the_stuff() { return get_output(); }
    protected:
    virtual void process(const doc_priv_t * d, std::string path) = 0;
    virtual std::string get_output() = 0;
};


struct doc_priv_t {
    using node_t = std::map<std::string, doc_priv_t>;
    using value_t = std::optional<std::string>;

    void add(std::string_view path, std::string value) {
        if (path.empty()) {
            _value = value;
            return;
        }

        auto path_elem = fetch_path_elem(path);
        if (path.empty()) {
            _nodes[std::string{path_elem}].add("", value);
            return;
        }

        _nodes[std::string{path_elem}].add(path, value);
    }

    void accept(base_visitor_t *visitor) const {
        accept(visitor, "");
    }

    std::string_view fetch_path_elem(std::string_view & path) {
        std::string_view result;

        auto slash_pos = path.find("/");
        if (slash_pos == std::string::npos) {
            result = path;
            path.remove_prefix(path.size());

            return result;
        }

        result = path.substr(0, slash_pos);
        path.remove_prefix(slash_pos + 1);
        return result;
    }

    node_t _nodes;
    value_t _value;

    protected:

    void accept(base_visitor_t *visitor, std::string path) const {
        visitor->do_the_stuff(this, path);

        for (auto &[k, v] : _nodes)
        {
            auto curr_path = path + "/" + k;
            v.accept(visitor, curr_path);
        };
    }

};

// --

struct printing_visitor_t : public base_visitor_t {
    void process(const doc_priv_t * d, std::string path) override {
        _ss << "path : " << path << " val: " << d->_value.value_or("N/A") << std::endl;
    }

    virtual std::string get_output() override {
        return _ss.str();
    }

    private:
    std::stringstream _ss;
};

// --

struct xml_visitor_t : public base_visitor_t {
    void process(const doc_priv_t * d, std::string path) override {
        std::cout << "xml visitor\n";
        if (d->_value.has_value())
            _ss << "<" << path << ">" << d->_value.value() << "</path>" << std::endl;
    }

    virtual std::string get_output() override {
        return _ss.str();
    }

    private:
    std::stringstream _ss;
};

// --

enum visitor_e : char{
    printing,
    xml,
    undef
};

static visitor_e str_to_visitor_e(const std::string &str) {
    if (str == "printing") return visitor_e::printing;
    if (str == "xml") return visitor_e::xml;
    return undef;
}

static std::unique_ptr<base_visitor_t> make_visitor(visitor_e v) {
    switch (v) {
        case visitor_e::printing : return std::make_unique<printing_visitor_t>();
        case visitor_e::xml      : return std::make_unique<xml_visitor_t>();
        default:
            throw doc::Error("unknown visitor type!");
    }
}

} // namespace doc